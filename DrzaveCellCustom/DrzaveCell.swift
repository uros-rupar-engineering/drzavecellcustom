//
//  DrzaveCell.swift
//  DrzaveCellCustom
//
//  Created by uros.rupar on 5/18/21.
//

import UIKit

class DrzaveCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

 
    @IBOutlet weak var ime: UILabel!
    
    @IBOutlet weak var zastava: UIImageView!
    @IBOutlet weak var glavniGrad: UILabel!
    @IBOutlet weak var brojStanovnika: UILabel!
}
