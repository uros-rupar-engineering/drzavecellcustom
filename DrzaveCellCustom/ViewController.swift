//
//  ViewController.swift
//  DrzaveCellCustom
//
//  Created by uros.rupar on 5/18/21.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource ,UITableViewDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    struct Drzave {
        var imeDrzave: String
        var zastava: String
        var glavniGrad: String
        let brojStanovnika: Int
    }
    let model = [
        Drzave(imeDrzave: "Spanija", zastava: "spania.png", glavniGrad: "madrid", brojStanovnika: 40000000),
        Drzave(imeDrzave: "Rusija", zastava: "russia.png", glavniGrad: "Moskva", brojStanovnika: 150000000)
    ]
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell")as? DrzaveCell
        
        cell?.zastava.image = UIImage(named: model[indexPath.row].zastava)
        cell?.ime.text = model[indexPath.row].imeDrzave
       
        
        return cell!
    }
    
    @IBOutlet weak var flag: UIImageView!
    
    @IBOutlet weak var imeIspis: UILabel!
    
    @IBOutlet weak var glGradIspis: UILabel!
    @IBOutlet weak var brStanovnikaIspis: UILabel!
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        flag.image = UIImage(named: model[indexPath.row].zastava)
        imeIspis.text = model[indexPath.row].zastava
        glGradIspis.text = model[indexPath.row].glavniGrad
        brStanovnikaIspis.text = String(model[indexPath.row].brojStanovnika)
        
    }
}

